import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { NgLightboxItemDirective } from './ng-lightbox-item.directive';
import { Observable, Subscription } from 'rxjs';
import { FastDomService } from '@silmar/ng-core/fastdom';

@Component({
  template  : `
    <div class="light-wrap" [class.light-opened]="opened" >
      <div class="light-in-wrap" [class.light-with-nav]="navigation" (click.self)="closeView.emit()" [prevent]="true"
           [stop]="true">
        <div *ngIf="navigation" class="light-nav" (click.stop)="prev.emit()">
          <div class="nav-left"></div>
        </div>
        <div class="light-image-wrap" [class.light-image-loading]="loading" (click.self)="closeView.emit()">
          <img [attr.src]="src" *ngIf="src" class="light-image" [attr.title]="title"
               (swipeleft)="next.emit()" (swiperight)="prev.emit()" (click.stop)="false" (load)="loading = false"/>
          <div class="light-image-caption" *ngIf="title">{{title}}</div>
        </div>
        <div *ngIf="navigation" class="light-nav" (click.stop)="next.emit()">
          <div class="nav-right"></div>
        </div>

        <div class="light-loader" *ngIf="loading"></div>
        <div class="light-close" (click.stop)="closeView.emit()"></div>
      </div>
      <div *ngIf="previews?.length" class="light-previews">
        <img *ngFor="let preview of previews" [attr.src]="preview.previewSrc" class="light-preview"
             (click)="change.emit(preview)" [attr.title]="preview.lightboxTitle" [class.current]="preview.current"/>
      </div>
    </div>
  `,
  styleUrls : [ 'ng-lightbox-view.component.scss' ]
})
export class NgLightboxViewComponent implements OnDestroy {
  @Input() navigation = true;

  @Input() previews: NgLightboxItemDirective[] = [];

  @Input() total: number;

  @Input() current: number;

  @Output() prev = new EventEmitter<void>();

  @Output() next = new EventEmitter<void>();

  @Output() change = new EventEmitter<NgLightboxItemDirective>();

  @Output() closeView = new EventEmitter<void>();

  title: string;

  src: string;

  loading: boolean;

  opened: boolean;

  protected preload$: Subscription;

  protected preloadSrc: string;

  constructor(protected cd: ChangeDetectorRef, protected dom: FastDomService, protected elRef: ElementRef) {}

  ngOnDestroy(): void {
    this.preload$ && this.preload$.unsubscribe();
  }

  close() {
    this.opened = false;
  }

  open(item: NgLightboxItemDirective) {
    this.opened = true;
    this.preload(item);
  }

  @Input() set item(item: NgLightboxItemDirective) {
    this.preload(item);
    this.scrollPreview(item.index);
  }

  protected preload(item: NgLightboxItemDirective, fn?: () => void) {
    if (this.src === item.getSrc() || this.preloadSrc === item.getSrc()) {
      return false;
    }

    this.preload$ && this.preload$.unsubscribe();

    this.loading    = true;
    this.preloadSrc = item.getSrc();
    this.preload$   = (new Observable(obs => {
      let handle;
      let img     = new Image(0, 0);
      const start = new Date().getTime();
      img.onload  = () => {
        this.dom.mutate(() => {
          this.current = item.index + 1;
          this.src     = item.getSrc();
          this.title   = item.lightboxTitle;
          obs.next(true);
          obs.complete();
          fn && fn();
          /*
          Fallback:
          - we will wait exactly the time needed to load this image and if we will set loading to false
          - this will cover the case where the image dom element never fires the onload event
           */
          handle = setTimeout(_ => {
            this.preloadSrc = null;
            this.loading = false;
            this.cd.markForCheck();
          }, (new Date().getTime()) - start);
          this.cd.markForCheck();
        });
      };
      img.onerror = () => {
        console.error('Cannot load image: ' + item.getSrc());
        obs.next(false);
        obs.complete();

        fn && fn();
        this.preloadSrc = null;
        this.loading = false;
        this.cd.markForCheck();
      };
      img.src     = item.getSrc();

      return () => {
        handle && clearTimeout(handle);
        img.onerror = null;
        img.onload  = null;
        img.src     = '';
        img         = null;
        handle      = null;
      };
    })).subscribe(res => res);
  }

  protected scrollPreview(index: number) {
    if (!this.previews?.length) {
      return false;
    }

    const element = this.elRef.nativeElement.querySelectorAll('img.light-preview')[ index ];

    if (element) {
      const parent = this.elRef.nativeElement.querySelector('div.light-previews');
      this.dom
        .mutate(() => parent.scroll(element.offsetLeft, element.offsetTop - parent.offsetTop));
    }
  }
}
