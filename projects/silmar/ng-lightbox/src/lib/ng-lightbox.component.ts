import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ComponentRef,
  ContentChildren,
  EmbeddedViewRef,
  HostListener,
  Inject,
  Injector,
  Input, OnDestroy,
  PLATFORM_ID,
  QueryList, ViewContainerRef
} from '@angular/core';
import { DOCUMENT, isPlatformServer } from '@angular/common';
import { NgLightboxItemDirective } from './ng-lightbox-item.directive';
import { Subscription } from 'rxjs';
import { NgLightboxViewComponent } from './ng-lightbox-view.component';

@Component({
  selector        : 'si-ng-lightbox',
  template        : `
    <ng-content></ng-content>`,
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class NgLightboxComponent implements AfterViewInit, OnDestroy {
  @ContentChildren(NgLightboxItemDirective, { descendants : true }) items: QueryList<NgLightboxItemDirective>;

  @Input() showNavigation = true;

  @Input() showPreviews = false;

  @Input() freezeBody = true;

  /**
   * Items change subscription
   */
  protected items$: Subscription;

  /**
   *
   */
  protected isServer: boolean;

  protected viewerRef: ComponentRef<NgLightboxViewComponent>;

  protected viewSubs$ = { prev : null, next : null, close : null, change : null };

  protected oldHeight: any;

  protected oldOverflow: any;

  constructor(@Inject(PLATFORM_ID) platformId, @Inject(DOCUMENT) protected doc: any, protected injector: Injector, protected viewContainerRef: ViewContainerRef) {
    this.isServer = isPlatformServer(platformId);
  }

  @HostListener('window:keyup', [ '$event' ])
  keyEvent(event: KeyboardEvent) {
    if (!this.isOpened()) {
      return true;
    }

    event.preventDefault();

    if (event.keyCode === 37 || event.key === 'ArrowLeft' || event.key === 'Left') {
      this.prev();
    } else if (event.keyCode === 39 || event.key === 'ArrowRight' || event.key === 'Right') {
      this.next();
    } else if (event.keyCode === 27 || event.key === 'Escape' || event.key === 'Esc') {
      this.close();
    }
  }

  ngOnDestroy(): void {
    this.close();
  }

  ngAfterViewInit() {
    if (!this.isServer) {
      this.items$ = this.items.changes.subscribe(() => this.init());

      this.init();
    }
  }

  isOpened() {
    return this.items.reduce((prev, item) => prev || item.isCurrent(), false);
  }

  open(item?: NgLightboxItemDirective) {
    item = item || this.items.first;

    this.close();

    this.viewerRef = this.viewContainerRef.createComponent(NgLightboxViewComponent, { injector : this.injector });

    this.initViewer();

    this.doc.body.appendChild((this.viewerRef.hostView as EmbeddedViewRef<any>).rootNodes[ 0 ] as HTMLElement);

    this.viewerRef.instance.open(item);
    this.changeItem(item);
    this.doFreezeBody(true);

    return true;
  }

  close() {
    if (!this.isOpened()) {
      return false;
    }

    this.items.map(item => item.setCurrent(false));

    this.viewSubs$.prev && this.viewSubs$.prev.unsubscribe();
    this.viewSubs$.next && this.viewSubs$.next.unsubscribe();
    this.viewSubs$.close && this.viewSubs$.close.unsubscribe();
    this.viewSubs$.change && this.viewSubs$.change.unsubscribe();

    this.viewerRef.instance.close();
    this.doFreezeBody(false);

    setTimeout(() => {
      this.viewerRef.destroy();
      this.viewerRef = null;
    }, 500);

    return true;
  }

  prev() {
    if (!this.isOpened() || !this.items.length) {
      return;
    }

    const items     = this.items.toArray();
    const prevIndex = items.findIndex(item => item.isCurrent()) - 1;

    return this.changeItem(prevIndex < 0 ? this.items.last : items[ prevIndex ]);
  }

  next() {
    if (!this.isOpened() || !this.items.length) {
      return;
    }

    const items     = this.items.toArray();
    const nextIndex = items.findIndex(item => item.isCurrent()) + 1;

    return this.changeItem(nextIndex >= this.items.length ? this.items.first : items[ nextIndex ]);
  }

  protected init() {
    let index = 0;
    this.items.map(item => item.init(this, index++));

    this.isOpened() && this.open(this.items.first);
  }

  protected changeItem(item: NgLightboxItemDirective) {
    this.viewerRef.instance.item = item;
    this.items.map(i => i.setCurrent(false));
    item.setCurrent(true);
  }

  protected doFreezeBody(freeze = true) {
    if (!this.freezeBody) {
      return;
    }

    if (freeze) {
      this.oldHeight   = this.doc.body.style.height;
      this.oldOverflow = this.doc.body.style.overflow;
    }

    this.doc.body.style.height   = freeze ? '100%' : this.oldHeight;
    this.doc.body.style.overflow = freeze ? 'hidden' : this.oldOverflow;
  }

  protected initViewer() {
    if (this.viewerRef && this.viewerRef.instance) {
      this.viewSubs$.prev                = this.viewerRef.instance.prev.subscribe(_ => this.prev());
      this.viewSubs$.next                = this.viewerRef.instance.next.subscribe(_ => this.next());
      this.viewSubs$.close               = this.viewerRef.instance.closeView.subscribe(_ => this.close());
      this.viewSubs$.change              = this.viewerRef.instance.change.subscribe(item => this.changeItem(item));
      this.viewerRef.instance.navigation = this.showNavigation && this.items.length > 1;
      this.viewerRef.instance.previews   = this.showPreviews && this.items.length > 1 ? this.items.toArray() : [];
      this.viewerRef.instance.total      = this.items.length;
    }
  }
}
