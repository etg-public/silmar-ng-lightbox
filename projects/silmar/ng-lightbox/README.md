# @silmar/ng-lightbox

[![npm (scoped)](https://img.shields.io/npm/v/@silmar/ng-lightbox.svg)](https://www.npmjs.com/package/@silmar/ng-lightbox)
[![pipeline status](https://gitlab.com/etg-public/silmar-ng-lightbox/badges/master/pipeline.svg)](https://gitlab.com/etg-public/silmar-ng-lightbox/commits/master)
[![NPM](https://img.shields.io/npm/l/@silmar/ng-lightbox.svg?style=flat-square)](https://www.npmjs.com/package/@silmar/ng-lightbox)

Really simple lightbox

### Install
```
npm i @silmar/ng-lightbox
// or
yarn add @silmar/ng-lightbox
```

### Usage
The very basic usage is as follows:

`main.ts`
```typescript
// ....
import 'hammerjs';
// ....
```

`app.module.ts`
```typescript
import {BrowserModule, HammerModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NgLightboxModule} from '@silmar/ng-lightbox';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HammerModule, // <-- For Angular 9
        NgLightboxModule // <-- import the carousel module
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
```
`app.component.html`
```angular2html
<si-ng-lightbox>
  <img *ngFor="let img of gallery" src="{{img}}" height="200" [siLightboxItem]="img" class="item"/>
</si-ng-lightbox>
```

### Demo
Visit the [demo](https://etg-public.gitlab.io/silmar-ng-lightbox/)
